const getVehicles = async () => {
  const response = await fetch(
    // update : size=10 modifier pour size=100
    "https://random-data-api.com/api/vehicle/random_vehicle?size=100"
  );
  const vehicles = await response.json();
  console.log("debug", vehicles);
  return vehicles;
};

export { getVehicles };
