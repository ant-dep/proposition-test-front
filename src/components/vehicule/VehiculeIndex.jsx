import React, { useEffect, useState } from "react";
import VehicleCard from "./VehicleCard";
import "./paginate.css";
import ReactPaginate from "react-paginate";
import { getVehicles } from "../../services/vehicles";
import styled from "styled-components";

const VehiculeIndex = () => {
  const [cars, setCars] = useState(null);
  // fix : let order = false devient un state
  const [order, setOrder] = useState(false);

  useEffect(() => {
    const fetchCars = async () => {
      setCars(await getVehicles());
    };
    fetchCars();
  }, []);

  // pagination setup
  const [page, setPage] = useState(0);
  const elementsPerPage = 12;
  const numberOfPagesVistited = page * elementsPerPage;
  // adpadt the number of elements per pag depending on orders
  const totalPages = Math.ceil(
    order
      ? cars?.filter((car) => car.color === "Black" || car.color === "White")
          .length / elementsPerPage
      : cars?.length / elementsPerPage
  );

  const changePage = ({ selected }) => {
    setPage(selected);
  };

  // display the cars and handle the order per page
  const displayPage = cars
    ?.slice(numberOfPagesVistited, numberOfPagesVistited + elementsPerPage)
    ?.map((car) => {
      if (order) {
        if (car.color === "Black" || car.color === "White") {
          return <VehicleCard key={car.id} {...car} />;
        }
      } else {
        return <VehicleCard key={car.id} {...car} />;
      }
    });

  if (!cars) {
    // fix : besoin d'une div à la place de <>Loading</>
    return <div>loading</div>;
  }

  return (
    <>
      {/* review: h1 au lieu d'un h3 -> SEO */}
      <h1>My garage</h1>
      <MainContainer>
        <ActionContainer order={order}>
          {/* fix : adpatation du order = false */}
          <button onClick={() => setOrder(!order)}>{`${
            order ? "Reset order" : "Only black & white"
          }`}</button>
        </ActionContainer>
        {/* rewiew: refacto du CardContainer et ajout d'une pagination passage */}
        <CardContainer>{displayPage}</CardContainer>
        {/* {cars.map((car) => {
            if (order) {
              // fix : du return apres le if -> return <> </>;
              if (car.color === "Black" || car.color === "White") {
                return <VehicleCard key={car.id} {...car} />;
              }
            } else {
              return <VehicleCard key={car.id} {...car} />;
            }
          })} */}
      </MainContainer>
      <ReactPaginate
        previousLabel={"<"}
        nextLabel={">"}
        pageCount={totalPages}
        onPageChange={changePage}
        containerClassName={"navigationButtons"}
        previousLinkClassName={"previousButton"}
        nextLinkClassName={"nextButton"}
        disabledClassName={"navigationDisabled"}
        activeClassName={"navigationActive"}
        marginPagesDisplayed={
          document.documentElement.clientWidth > 425 ? 2 : 0
        } /* hide page numbers on smaller device */
        pageRangeDisplayed={document.documentElement.clientWidth > 425 ? 2 : 0}
      />
    </>
  );
};

export default VehiculeIndex;

// Review : déplacés en bas pour plus de lisibilité
const MainContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  width: 100%;
  border-bottom: 1px solid #ebebeb;
  padding: 10px 0;
`;

const ActionContainer = styled.div`
  margin-bottom: 1rem;
  & button {
    cursor: pointer;
    border: 1px solid #000;
    border-radius: 0.25rem;
    padding: 0.5rem 1rem;
    background-color: ${(props) => (props.order ? "#fff" : "#000")};
    color: ${(props) => (props.order ? "#000" : "#fff")};
    &:active {
      transform: scale(0.96);
    }
  }
`;

const CardContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(300px, 1fr));
  grid-gap: 1rem;
  margin-bottom: 1rem;
  width: 100%;
`;
