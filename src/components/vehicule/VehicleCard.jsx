import styled from "styled-components";

// rewiew : let transformé en const
const VehicleCard = (props) => {
  return (
    <VehicleContainer {...props}>
      <h3>
        {props.make_and_model} <p>{props.year}</p>
      </h3>
      {/* rewiew : inutile car minimum 2portes */}
      {/* {props.doors < 2 && <p>{props.doors} door</p>} */}
      <MainInfoContainer>
        <p>{props.transmission}</p>
        <p>{props.doors} doors</p>
        <p>{props.color}</p>
      </MainInfoContainer>
      <BottomContainer>
        <p>{props.kilometrage} kms</p>
        <small>ref. {props.id}</small>
      </BottomContainer>
    </VehicleContainer>
  );
};

export default VehicleCard;

// Review : déplacés en bas pour plus de lisibilité
const VehicleContainer = styled.div`
  cursor: pointer;
  flex: 0 0 30%;
  border: 1px solid #000;
  border-radius: 0.25rem;
  padding: 0.5rem 1rem;
  margin-bottom: 0.5rem;
  box-shadow: 0 0 5px ${(props) => props.color?.toLowerCase()};
  transition: all 0.2s ease-in-out;
  &:hover {
    transform: scale(1.02);
  }
  & h3 {
    border-bottom: 1px solid #ebebeb;
    margin-bottom: 0.5rem;
  }
`;

const MainInfoContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 0.5rem;
  border-bottom: 1px solid #ebebeb;
`;

const BottomContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;
