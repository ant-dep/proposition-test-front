import { ThemeProvider } from "styled-components";
import VehiculeIndex from "./components/vehicule/VehiculeIndex";
import theme from "./theme";
import styled from "styled-components";

function App() {
  return (
    <AppContainer>
      <ThemeProvider theme={theme}>
        <VehiculeIndex />
      </ThemeProvider>
    </AppContainer>
  );
}

export default App;

const AppContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
  height: 100vh;
  padding: 20px 40px;
  max-width: 1200px;
  margin: 0 auto;
`;
